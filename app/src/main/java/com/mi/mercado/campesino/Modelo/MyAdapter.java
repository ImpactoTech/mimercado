package com.mi.mercado.campesino.Modelo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mi.mercado.campesino.R;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{
    private List<Pedido> listData;

    public MyAdapter(List<Pedido> listData) {
        this.listData = listData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_data,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Pedido datos = listData.get(position);
        holder.codigo_pedido.setText(datos.getCodigo_pedido());
        holder.nombre.setText(datos.getNombre());
        holder.cantidad.setText(String.valueOf(datos.getCantidad()));
        holder.direccion.setText(datos.getDireccion());
        holder.fecha.setText(datos.getFecha());
        holder.estado.setText(datos.getEstado());


    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView codigo_pedido,nombre, cantidad, direccion, fecha, estado;
        public ViewHolder(View itemView) {
            super(itemView);
            codigo_pedido = itemView.findViewById(R.id.codigo_pedido);
            nombre = itemView.findViewById(R.id.nombre);
            cantidad = itemView.findViewById(R.id.cantidad);
            direccion = itemView.findViewById(R.id.direccion);
            fecha = itemView.findViewById(R.id.fecha);
            estado = itemView.findViewById(R.id.estado);

        }
    }
}
