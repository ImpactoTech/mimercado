package com.mi.mercado.campesino.ui.home;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mi.mercado.campesino.Modelo.Pedido;
import com.mi.mercado.campesino.R;
import com.squareup.picasso.Picasso;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HomeFragment extends Fragment {
    private ImageView imgMercado, imgSumar, imgRestar;
    private Button mBtnComprar, mBtnCompletarPedido;
    private EditText mNombre, mDireccion, mCorreo, mNumero, mObservaciones;
    private HomeViewModel homeViewModel;
    private FirebaseAuth mAuth;
    private String UID = "", nombre, direccion,correo,numero,observaciones, TAG = "error";
    private int cantidad = 0;
    private TextView txtCantidad, mAdvertencia;
    private  DatabaseReference myCantidad, myRef;
    private int cont =  0;


    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        imgMercado = root.findViewById(R.id.imgMercado);
        mBtnComprar = root.findViewById(R.id.mBtnComprar);
        txtCantidad = root.findViewById(R.id.txtCantidad);
        imgSumar = root.findViewById(R.id.imageSumar);
        imgRestar = root.findViewById(R.id.imageRestar);
        Picasso.get().load("https://fotos.subefotos.com/0ea340e083b5e05479e5651b3b34bceao.png").into(imgMercado);


        imgSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtCantidad.setText(String.valueOf(Integer.parseInt(txtCantidad.getText().toString()) + 1));
            }
        });


        imgRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Integer.parseInt(txtCantidad.getText().toString()) - 1  != 0){
                    txtCantidad.setText(String.valueOf(Integer.parseInt(txtCantidad.getText().toString()) - 1));
                }

            }
        });


        mBtnComprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirDialogVentas();
            }
        });

        imgMercado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirListaProductos();
            }
        });


        return root;
    }

    private void abrirDialogVentas() {
        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        View mView = getLayoutInflater().inflate(R.layout.layout_dialog, null);

        mNombre = mView.findViewById(R.id.edtNombre);
        mDireccion = mView.findViewById(R.id.edtDireccion);
        mCorreo = mView.findViewById(R.id.edtCorreo);
        mNumero = mView.findViewById(R.id.edtNumero);
        mObservaciones = mView.findViewById(R.id.edtObservaciones);
        mBtnCompletarPedido = mView.findViewById(R.id.mBtnCompletarPedido);
        mAdvertencia = mView.findViewById(R.id.advertencia);
        mAdvertencia.setVisibility(View.INVISIBLE);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        UID = currentUser.getUid();




        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        mBtnCompletarPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mNombre.getText().toString().trim().isEmpty() || !mDireccion.getText().toString().trim().isEmpty()  || !mNumero.getText().toString().trim().isEmpty()){
                // Write a message to the database
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                myRef = database.getReference("Pedidos").child(UID);
                myCantidad = database.getReference("CantidadComprada");



                nombre = mNombre.getText().toString().trim();
                direccion = mDireccion.getText().toString().trim();
                correo = mCorreo.getText().toString().trim();
                numero = mNumero.getText().toString().trim();
                observaciones = mObservaciones.getText().toString();
                cantidad = Integer.parseInt(txtCantidad.getText().toString());

                String mGroupId = myRef.push().getKey();
                myRef.child(mGroupId).setValue(new Pedido(mGroupId, nombre, direccion,correo,numero,observaciones,cantidad, getDateTime(), "Pedido tomado"));
                actualizarCantidadVendida(cantidad);

                AbrirNumeroPedido(mGroupId);
                dialog.dismiss();
                }
                else{
                    mAdvertencia.setVisibility(View.VISIBLE);
                }


            }
        });
    }


    public void AbrirNumeroPedido(String numero_pedido){
        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        View mView = getLayoutInflater().inflate(R.layout.layout_numero_pedido, null);

        TextView numeroPedido =mView.findViewById(R.id.numero_pedido);
        numeroPedido.setText(numero_pedido);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
    }

    public void abrirListaProductos(){
        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        View mView = getLayoutInflater().inflate(R.layout.layout_lista_productos, null);


        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
    }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void actualizarCantidadVendida(final int cantidadComprada){
        myCantidad.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Long cantidad = dataSnapshot.getValue(Long.class);
                if(cantidad == null){
                    myCantidad.setValue(0);
                }else{
                    myCantidad.setValue(cantidad + (10*cantidadComprada));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }

        });
    }
}