package com.mi.mercado.campesino.Modelo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mi.mercado.campesino.R;

import java.util.List;

public class AdapterProductor extends RecyclerView.Adapter<AdapterProductor.ViewHolder>{
    private List<Productores> listData;

    public AdapterProductor(List<Productores> listData) {
        this.listData = listData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_productores,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Productores datos = listData.get(position);
        holder.finca.setText(datos.getFinca());
        holder.nombre.setText(datos.getProductor());



    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView finca, nombre;
        public ViewHolder(View itemView) {
            super(itemView);
            finca = itemView.findViewById(R.id.finca);
            nombre = itemView.findViewById(R.id.nombre);


        }
    }
}
