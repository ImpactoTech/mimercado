package com.mi.mercado.campesino.Modelo;

public class Productores {

    private String codigo;
    private String Finca;
    private String Productor;

    public Productores() {
    }

    public Productores(String codigo, String finca, String productor) {
        this.codigo = codigo;
        Finca = finca;
        Productor = productor;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getFinca() {
        return Finca;
    }

    public void setFinca(String finca) {
        Finca = finca;
    }

    public String getProductor() {
        return Productor;
    }

    public void setProductor(String productor) {
        Productor = productor;
    }
}
