package com.mi.mercado.campesino.ui.dashboard;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.mi.mercado.campesino.CaptureAct;
import com.mi.mercado.campesino.Modelo.AdapterProductor;
import com.mi.mercado.campesino.Modelo.Productores;
import com.mi.mercado.campesino.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends Fragment {

    private TextView cantidadComprada;
    private DatabaseReference myCantidad;
    private String TAG = "";
    private List<Productores> listData;
    private RecyclerView recyclerView;
    private AdapterProductor adapter;
    private  DatabaseReference myRef;
    private Button mBtnLeerCodigo;

    private View root;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_dashboard, container, false);



        cantidadComprada = root.findViewById(R.id.cantidad_comprada);
        recyclerView = root.findViewById(R.id.recyclerview);
        mBtnLeerCodigo = root.findViewById(R.id.mBtnLeerCodigo);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        listData = new ArrayList<>();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Productores");
        myCantidad = database.getReference("CantidadComprada");
        recuperarCantidad();
        mBtnLeerCodigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScanCode();
            }
        });
       // myRef.push().setValue(new Productores("1011","Los árboles", "Juan Carlos Lopez"));
        return root;
    }

    public void ScanCode(){
        IntentIntegrator integrator = new IntentIntegrator(getActivity());
        integrator.setCaptureActivity(CaptureAct.class);
        integrator.setOrientationLocked(false);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Leer código del costal");
        integrator.forSupportFragment(DashboardFragment.this).initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null){

            if(result.getContents() != null){
                mostrarProductores(result.getContents());
            }
        }
    }

    public void mostrarProductores(String codigoCostal){
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                listData.clear();

                if (dataSnapshot.exists()){
                    for (DataSnapshot npsnapshot : dataSnapshot.getChildren()){
                        Productores productor = npsnapshot.getValue(Productores.class);
                        listData.add(productor);
                    }
                    adapter=new AdapterProductor(listData);
                    recyclerView.setAdapter(adapter);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());

            }
        };
        myRef.orderByChild("codigo").equalTo(codigoCostal).addValueEventListener(postListener);
    }





    public void recuperarCantidad(){

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Long cantidad = dataSnapshot.getValue(Long.class);
                if (cantidad != null){
                    cantidadComprada.setText(String.valueOf(cantidad));
                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        myCantidad.addValueEventListener(postListener);
    }
}