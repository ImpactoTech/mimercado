package com.mi.mercado.campesino.Modelo;

public class Pedido {
    private String codigo_pedido;
    private String Nombre;
    private String Direccion;
    private String Correo;
    private String Celular;
    private String Observaciones;
    private int Cantidad;
    private String fecha;
    private String estado;

    public Pedido() {
    }

    public Pedido(String codigo_pedido, String nombre, String direccion, String correo, String celular, String observaciones, int cantidad, String fecha, String estado) {
        this.codigo_pedido = codigo_pedido;
        Nombre = nombre;
        Direccion = direccion;
        Correo = correo;
        Celular = celular;
        Observaciones = observaciones;
        Cantidad = cantidad;
        this.fecha = fecha;
        this.estado = estado;
    }

    public String getCodigo_pedido() {
        return codigo_pedido;
    }

    public void setCodigo_pedido(String codigo_pedido) {
        this.codigo_pedido = codigo_pedido;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public String getCelular() {
        return Celular;
    }

    public void setCelular(String celular) {
        Celular = celular;
    }

    public String getObservaciones() {
        return Observaciones;
    }

    public void setObservaciones(String observaciones) {
        Observaciones = observaciones;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int cantidad) {
        Cantidad = cantidad;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
