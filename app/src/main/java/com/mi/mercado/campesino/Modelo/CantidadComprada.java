package com.mi.mercado.campesino.Modelo;

public class CantidadComprada {
    private Long Cantidad;

    public CantidadComprada() {
    }

    public CantidadComprada(Long cantidad) {
        Cantidad = cantidad;
    }

    public Long getCantidad() {
        return Cantidad;
    }

    public void setCantidad(Long cantidad) {
        Cantidad = cantidad;
    }
}
