package com.mi.mercado.campesino.ui.notifications;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mi.mercado.campesino.Modelo.AdapterProductor;
import com.mi.mercado.campesino.Modelo.MyAdapter;
import com.mi.mercado.campesino.Modelo.Pedido;
import com.mi.mercado.campesino.R;

import java.util.ArrayList;
import java.util.List;

public class NotificationsFragment extends Fragment {


    private FirebaseAuth mAuth;
    private String UID, TAG = "error";
    private  DatabaseReference myRef;
    private List<Pedido> listData;
    private RecyclerView recyclerView;
    private MyAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_notifications, container, false);


        recyclerView = root.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        listData=new ArrayList<>();


        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        UID = currentUser.getUid();

        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Pedidos").child(UID);


        recuperarPedidos();

        return root;
    }

    public void recuperarPedidos(){
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI

                if (dataSnapshot.exists()){
                    for (DataSnapshot npsnapshot : dataSnapshot.getChildren()){
                       Pedido pedido=npsnapshot.getValue(Pedido.class);
                        listData.add(pedido);
                    }
                    adapter=new MyAdapter(listData);
                    recyclerView.setAdapter(adapter);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());

            }
        };
        myRef.addValueEventListener(postListener);
    }
}